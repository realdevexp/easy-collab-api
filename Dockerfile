FROM node:14-alpine

LABEL version="1.0"
LABEL description="This is the base docker image for the Project Management Tool backend API."
LABEL maintainer = ["info@realdev.in"]

WORKDIR /app

COPY ["package.json", "yarn.lock", "./"]
RUN ls
RUN yarn install --production
COPY . .

EXPOSE 5000

RUN npx prisma generate 

CMD ["yarn", "prod"]