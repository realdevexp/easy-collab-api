# project-management-tool-api

### Development

✅ `git clone clone-path`

✅ Run `yarn install` to install all the dependencies.

✅ Run `yarn dev` to run production server.

✅ Run `yarn lint` to check linter's errors.

✅ Run `yarn test` to run testing server.

### make sure you change `config/config.json` according to your database access data

<hr />

### Docker Setup (Without Postgres DB)

✅ Run `docker build -t easycollab:backend .` to build docker image.
✅ Run `docker run -p 5000:5000 easycollab:backend` to run docker image.
✅ Now you should be able to visit [API-Docs](http://localhost:5000/api-docs).

### Docker Setup (With Postgres DB)

- Update `POSTGRES_DB` `POSTGRES_USER` `POSTGRES_PASSWORD` with your new credentials in `.env` and also update `DATABASE_URL` with these new credentials. Your `DB_HOST` will be postgres container name which is `postgres` in this case. (Check docker-compose.yml file)

✅ Run `docker compose build` to build docker container (This will take sometime during first time run).
✅ Run `docker compose up` to run docker container.
✅ Now you should be able to visit [API-Docs](http://localhost:5000/api-docs).
✅ Run `docker compose down` to stop docker container.

#### Wish to run only Postgres DB from docker?

- Instead of running `docker compose up` you can run `docker compose start postgres`, this will run only postgres image.
✅ Now you should be able to use postgres db with the credentials you provided in `.env` file. This time your 
`DB_HOST` will be `localhost`, So make sure you update it in `DATABASE_URL` as well.

#### How you can use Logger
Whenever you need to log something on your terminal to debug something, use the logger service by following the below-given steps:

1) **Import the logger service at the top of your working file** as logger from the services folder present at the root level of this project.

2) After importing, **you can use various methods present on it to determine the type of log** you want to see in the terminal like error, info, fatal, etc. You can check all the options in the loggerService file itself.

Something like this:-
```javascript
logger.info('Login route has been accessed!!');
```
Don't forget to write a friendly message within the parentheses.

##### Wish not to see the JSON response ?

Now, by default, **you will also see a JSON response for that particular request along with the message** or data you passed in the parentheses. 
But, **if you wish not to see the default JSON response, you can set the autoLogging property to false in the server.js file at Line 22** (also present in the root directory).

##### Wish to store your logs in the 'log' file ?
At last, if you wish to store these logs in the logs file. **All you have to do is to set the 'env' variable named "LOGGER" = 'file'.** Now, all the logs will be automatically stored in the log file.
