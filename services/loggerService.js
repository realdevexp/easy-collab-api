import pino from 'pino';
import path from 'path';
const __dirname = path.resolve();

// These are the custom levels, which depicts the severity and urgency of the message
const levels = {
    http: 10,
    debug: 20,
    info: 30,
    warn: 40,
    error: 50,
    fatal: 60,
};

let logger;

if(process.env.LOGGER === 'file'){
    logger = pino(
        {
            customLevels: levels,        // our defined levels
            useOnlyCustomLevels: true,   
            level: 'http',               // default level
            prettyPrint: {
                colorize: true,          // colorizes the log
                levelFirst: true,
                translateTime: 'yyyy-dd-mm, h:MM:ss TT',           
            },                
        },
        pino.destination(`${__dirname}/error.log`)
    );
} else{
    logger = pino(
        {
            customLevels: levels,        // our defined levels
            useOnlyCustomLevels: true,   
            level: 'http',               // default level
            prettyPrint: {
                colorize: true,          // colorizes the log
                levelFirst: true,
                translateTime: 'yyyy-dd-mm, h:MM:ss TT',           
            },                
        }
    );
}


export default logger;