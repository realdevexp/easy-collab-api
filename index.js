import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import loaders from './loaders';

const PORT = process.env.PORT || 5000;

(async () => {
    const app = express();
    try {
        await loaders({ expressApp: app, });
    } catch (error) {
        console.log(error);
        // eslint-disable-next-line no-process-exit
        process.exit(130);
    }

    app.listen(PORT, () => {
        console.log('[APP]');
        console.log(`> Server listening on port: ${PORT} <`);
        console.log(`> API Docs: http://localhost:${PORT}/api-docs <`);
    }).on('error', (err) => {
        throw new Error(err);
    });
})();
