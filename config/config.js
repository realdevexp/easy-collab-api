import dotenv from 'dotenv';
dotenv.config();
export default {
    MAILER_EMAIL_ID: process.env.MAILER_EMAIL_ID,
    MAILER_PASSWORD: process.env.MAILER_PASSWORD,
    MAILER_HOST: process.env.MAILER_HOST,
    NODE_ENV: process.env.NODE_ENV,
    tokensecret: process.env.tokensecret,
    GOOGLE_PROJECT_ID: process.env.GOOGLE_PROJECT_ID,
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    GITHUB_CLIENT: process.env.GITHUB_CLIENT,
    GITHUB_TOKEN: process.env.GITHUB_TOKEN,
    PORT: process.env.PORT,
    expires_in: process.env.expires_in,
    DATABASE_URL: process.env.DATABASE_URL,
    SHADOW_DATABASE_URL: process.env.SHADOW_DATABASE_URL,
};
