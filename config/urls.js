const URLS = (env) => {
    return {
        googleAuth: {
            googleAuthScreenUrl: `https://accounts.google.com/o/oauth2/v2/auth?client_id=${env.GOOGLE_CLIENT_ID}&redirect_uri=http://localhost:8080/oauth/signin/google&response_type=code&scope=https://www.googleapis.com/auth/userinfo.profile email`,
            getTokenUrl: `https://oauth2.googleapis.com/token?client_id=${env.GOOGLE_CLIENT_ID}&client_secret=${env.GOOGLE_CLIENT_SECRET}&grant_type=authorization_code&redirect_uri=http://localhost:8080/oauth/signin/google`,
            getUserinfoUrl: 'https://www.googleapis.com/oauth2/v3/userinfo',
        },
        githubAuth: {
            GithubAuthScreenUrl: `https://github.com/login/oauth/authorize?scope=user:email&client_id=${env.GITHUB_CLIENT}`,
            getTokenUrl: `https://github.com/login/oauth/access_token?client_id=${env.GITHUB_CLIENT}&client_secret=${env.GITHUB_TOKEN}`,
            getUserinfoUrl: 'https://api.github.com/user',
            getUserEmailUrl: 'https://api.github.com/user/emails',
        },
    };
};

export default URLS;

