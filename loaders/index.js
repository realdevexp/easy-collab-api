import expressLoader from './express';
import swaggerLoader from './swagger';
import env from '../config/config';

const checkEnv = () => {
    console.log('Checking for environment variables...');
    for (const [ key, value ] of Object.entries(env)) {
        if(!value){
            throw new Error(`Env variable '${key}' is not defined, please define it in .env file.`);
        }
    }
};

export default async ({ expressApp, }) => {

    checkEnv();
    
    expressLoader({ app: expressApp, });
    console.log('[MODULE] Express Initialized.');

    await swaggerLoader({ app: expressApp, });
    console.log('[MODULE] Swagger Initialized.');
};