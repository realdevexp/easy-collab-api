import { createContainer, asClass, asValue, InjectionMode } from 'awilix';
import AuthController from '../controllers/authController/authController';
import PasswordController from '../controllers/authController/passwordController';
import AuthService from '../services/authService';
import MailerService from '../services/mailerService';
import OAuthService from '../services/oauthService';
import prisma from '../prisma';
import env from '../config/config';

// Create the container and set the injectionMode to PROXY (which is also the default).
const container = createContainer({
    injectionMode: InjectionMode.PROXY,
});

container.register({
    // Here we are telling Awilix how to resolve a
    // authController: by instantiating a class.
    AuthController: asClass(AuthController),
    PasswordController: asClass(PasswordController),
    AuthService: asClass(AuthService),
    MailerService: asClass(MailerService),
    OAuthService: asClass(OAuthService),

    prisma: asValue(prisma),
    env: asValue(env),
});

console.log('[MODULE] Awilix Initialized.');

export {
    container,
    env
};